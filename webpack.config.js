const path = require('path');
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
  entry: './src/index.js',
  output: {
    library: 'E3MapboxDraw',
    libraryTarget: 'umd',
    libraryExport: 'default',
    path: path.resolve(__dirname, 'dist'),
    filename: 'index.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
    ],
  },
  resolve: {
    alias: {
      // mapbox-gl related packages in webpack should use dist instead of the default src
      '@mapbox/mapbox-gl-draw': path.resolve(__dirname, 'node_modules/@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.js'),
    },
  },
  plugins: [
    new UglifyJsPlugin(),
    new webpack.HotModuleReplacementPlugin(),
  ],
};
