const addProperty = (obj, prop, value) => {
  if (obj[prop]) {
    console.warn(`Beware the property ${prop} already exists and is going to \
be overwritten`);
  }
  obj[prop] = value; // eslint-disable-line
  //                    OH GOD WHAT DID I DO !!!
};

module.exports = {
  addProperty,
};
