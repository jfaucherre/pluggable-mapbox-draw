const requiredField = (field) => {
  throw new Error(`Missing field ${field} for UserTrackingPlugin constructor`);
};

const PLUGIN_NAME = 'MapboxDrawUserTrackingPlugin';

export default class UserTrackingPlugin {
  constructor({
    user = requiredField('user'),
    email = requiredField('email'),
  }) {
    this.name = PLUGIN_NAME;
    this.user = { user, email };
  }

  onAdd(map, draw) {
    this._map = map;
    this._draw = draw;

    const { user } = this;

    ['draw.create', 'draw.update'].forEach((event) => {
      this._map.on(event, ({ features }) => {
        features.forEach((feature) => {
          this.setFeatureProperty(
            feature.id,
            'userHistory',
            [
              ...(feature.properties.userHistory || []),
              {
                user: user.user,
                email: user.email,
                time: (new Date()).toISOString(),
                action: event,
              },
            ],
          );
        });
      });
    });
  }
}
