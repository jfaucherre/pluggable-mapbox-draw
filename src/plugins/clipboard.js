import { isEmpty } from 'ramda';
import bearing from '@turf/bearing';
import center from '@turf/turf';
import distance from '@turf/distance';
import transformTranslate from '@turf/transform-translate';
import { featureCollection, point } from '@turf/helpers';
import { addProperty } from './utils';

const PLUGIN_NAME = 'MapboxDrawClipboardPlugin';
const EMPTY_CLIPBOARD = {
  content: {},
  nbPasted: 0,
  lastClick: null,
};

export default class ClipboardPlugin {
  constructor() {
    this.name = PLUGIN_NAME;
  }

  onAdd(map, draw) {
    this._map = map;
    this._draw = draw;
    this._clipboard = EMPTY_CLIPBOARD;

    addProperty(this._draw, 'copy', this.copy);
    addProperty(this._draw, 'paste', this.paste);
  }

  onRemove() {
    this._clipboard = undefined;
    this._map = undefined;
    this._draw = undefined;
  }

  copy() {
    const selected = this._drawer.getSelected();

    this._clipboard = {
      content: JSON.parse(JSON.stringify(selected)),
      nbPasted: 0,
      lastClick: null,
    };
  }

  paste(event = {}) {
    if (isEmpty(this._clipboard.content)) {
      return;
    }

    const { content: features, lastClick } = this._clipboard;
    // Get center of old features
    const featuresCenter = center(features);
    // Translate the center to the paste destination
    // if no destination click, place them so that they don't overlap
    // else place them on the click
    let copyTo;

    if (lastClick === null) {
      this._clipboard.nbPasted = this._clipboard.nbPasted + 1;
      copyTo = transformTranslate(
        featuresCenter,
        20 * this._clipboard.nbPasted,
        45,
        { units: 'meters' },
      );
    } else {
      copyTo = point([lastClick.lng, lastClick.lat]);
    }
    // Reset _clipboard destination
    this._clipboard.lastChild = null;
    // Translate the features to the paste direction
    const newFeatures = featureCollection(features
      .map(feature => transformTranslate(
        feature,
        distance(featuresCenter, copyTo, {}),
        bearing(featuresCenter, copyTo, {}),
      )));
    // Fire the created event with the translated features
    this._map.fire('draw.create', {...event,features: newFeatures});
  }
}
