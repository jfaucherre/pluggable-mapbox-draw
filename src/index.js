import MapboxDraw from '@mapbox/mapbox-gl-draw';

/**
 * This class inherits of MapboxDraw but enable user to add plugins
 *
 * The schema of a plugin is
 * {
 *   name: String,
 *   onAdd: (Map, this) -> (),
 *   onRemove: () -> ()
 * }
 */
class PluggableMapboxDraw extends MapboxDraw {
  constructor(config = {}) {
    const { plugins = [], ...remains } = config;
    super(remains);
    this.plugins = plugins;

    const superOnAdd = this.onAdd;
    const superOnRemove = this.onRemove;

    this.onAdd = (map) => {
      this.map = map;
      this.plugins.map(plugin => plugin.onAdd(map, this));
      return superOnAdd(map);
    };
    this.onRemove = () => {
      this.plugins.map(plugin => plugin.onRemove());
      return superOnRemove();
    };
  }

  _findPluginIndex = (plugin) => {
    const { id } = plugin;
    return this.plugins.findIndex(({ id: i }) => i === id);
  }

  addPlugin = (plugin) => {
    const inside = this.findPlugin(plugin);

    if (inside) {
      throw new Error(`Plugin ${plugin.name} is already present`);
    }
    this.plugins.concat(plugin);
    if (this.map) {
      plugin.onAdd(this.map, this);
    }
  }

  removePlugin = (plugin) => {
    const index = this._findPluginIndex(plugin);

    if (index === -1) {
      throw new Error(`Plugin ${plugin.name} is not present`);
    }
    this.plugins[index].onRemove();
    this.plugins = [
      ...this.plugins.slice(0, index),
      ...this.plugins.slice(index + 1),
    ];
  }
}

export default PluggableMapboxDraw;
