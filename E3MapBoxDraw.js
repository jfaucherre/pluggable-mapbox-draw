import MapBoxDraw from '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw';
import '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css';
import './map.css';
import getCenter from '@turf/center';
import transformTranslate from '@turf/transform-translate';
import distance from '@turf/distance';
import bearing from '@turf/bearing';
import envelope from '@turf/envelope';
import * as turfHelpers from '@turf/helpers';
import union from '@turf/union';
import difference from '@turf/difference';

class E3MapBoxDraw extends MapBoxDraw {
  addFeatures({ map, features, user }) {
    this.map = map;
    if (features.includes('history')) {
      this.addUndoRedo(map);
    }
    if (features.includes('userTracking')) {
      this.addUserTracking(map, user);
    }
    if (features.includes('finishedAreas')) {
      this.finishedAreas = true;
      this.addFinishedAreas(map);
    }
  }

  addUndoRedo(map) {
    // This function unables the UndoRedo feature
    this.history = { past: [], future: [], selection: [] };
    let lastEvent;
    document.addEventListener('keyup', (e) => {
      if (e.ctrlKey) {
        switch (e.key) {
          // We revert the event, the situation is symmetrical
          case 'z':
            lastEvent = this.history.past.pop();
            if (lastEvent !== undefined) {
              this.history.future.push(this.revertEvent(lastEvent, this));
            }
            break;
          case 'y':
            lastEvent = this.history.future.pop();
            if (lastEvent !== undefined) {
              this.history.past.push(this.revertEvent(lastEvent, this));
            }
            break;
          default:
            break;
        }
      }
    }, { passive: true });
    ['draw.create', 'draw.update', 'draw.delete'].forEach((event) => {
      map.on(event, (features) => {
        console.log(features);
        // We limit at 20 recorded operations
        this.history.past.splice(0, this.history.past.length - 20);
        if (event === 'draw.update') {
          this.history.past.push({ event, features: JSON.parse(JSON.stringify(this.history.selection)) });
        } else {
          this.history.past.push({ event, features: JSON.parse(JSON.stringify(features.features)) });
        }
        this.history.future = [];
      });
    });
    map.on('draw.selectionchange', (features) => {
      this.history.selection = JSON.parse(JSON.stringify(features.features));
    });
  }

  revertEvent(lastEvent) {
    console.log('Event to revert :', lastEvent);
    try {
      switch (lastEvent.event) {
        case 'draw.create':
          lastEvent.features.forEach(feature => this.delete(feature.id));
          return ({ event: 'draw.delete', features: lastEvent.features });
        case 'draw.delete':
          lastEvent.features.forEach(feature => this.add(feature));
          return ({ event: 'draw.create', features: lastEvent.features });
        case 'draw.update':
          return ({
            event: 'draw.update',
            features: lastEvent.features.map(
              (feature) => {
                const f = JSON.parse(JSON.stringify(this.get(feature.id)));
                console.log(f);
                console.log(feature);
                this.delete(feature.id).add(feature);
                return f;
              },
            ),
          });
      }
    } catch (e) {
      console.log('Impossible to revert event, error :', e);
    }
  }

  addUserTracking(map, user) {
    ['draw.create', 'draw.update'].forEach((event) => {
      map.on(event, (features) => {
        features.features.forEach((feature) => {
          this.setFeatureProperty(feature.id, 'userHistory', [...(feature.properties.userHistory || []), {
            user: user.user,
            email: user.email,
            time: (new Date()).toISOString(),
            action: event,
          }]);
        });
      });
    });
  }

  addFinishedAreas(map) {
    console.log('FinishedAreas toggled');
    let mouseDragStart = {};
    map.on('mousedown', (e) => {
      mouseDragStart = { point: e.point, lngLat: e.lngLat };
    });

    // FinishedArea feature is triggered on a mouseup with shift+alt
    map.on('mouseup', (e) => {
      // if Shift+Alt is pressed during a drag, we save the modification of the progression area
      console.log(e);
      if (e.originalEvent && e.originalEvent.shiftKey && e.originalEvent.altKey && !(e.point.equals(mouseDragStart.point))) {
        console.log(this.getSelected());

        console.log(e);
        const newFinishedArea = envelope(
          turfHelpers.featureCollection([
            turfHelpers.point(
              [e.lngLat.lng, e.lngLat.lat],
            ),
            turfHelpers.point(
              [mouseDragStart.lngLat.lng, mouseDragStart.lngLat.lat],
            ),
          ]),
        );
        const finishedAreaSource = map.getSource('finishedArea');
        const oldFinishedArea = finishedAreaSource._data;
        // Depending on the mode, we either add or remove the selection
        if (oldFinishedArea.geometry.coordinates[0].length <= 0) {
          // If the saved zone was empty, we directly save
          finishedAreaSource.setData(newFinishedArea);
        } else {
          // Otherwise, we can make the operation
          if (this.state.finishSavingModeAdd) {
            // Union mode
            finishedAreaSource.setData(union(oldFinishedArea, newFinishedArea));
          } else {
            // Difference mode
            // As we can't save a null as geojson if the user completely deleted the savedzone, we put an empty one.
            finishedAreaSource.setData(difference(oldFinishedArea, newFinishedArea) || {
              type: 'Feature',
              geometry: { type: 'Polygon', coordinates: [[]] },
            });
          }
        }
        // Firing an event to reload Save Button
        map.fire('draw.finishedAreaChange');
      }
    });
  }

  finishedAreasSwitch() {
    this.finishSavingModeAdd = !this.finishSavingModeAdd;
  }

  updateGeojson(map, geojson) {
    // Set the content (more or less equivalent to delete all and add all)
    this.set({
      type: 'FeatureCollection',
      features: geojson.features.filter(f => (f.geometry.type === 'Polygon' && f.geometry.coordinates[0].length >= 4)),
    });

    // Reset history (to prevent errors)
    this.history = { past: [], future: [], selection: [] };

    if (this.finishedAreas) {
      if (map.getSource('finishedArea')) {
        map.removeLayer('finishedArea');
        map.removeSource('finishedArea');
      }
      map.addSource('finishedArea',
        {
          type: 'geojson',
          data: geojson.finishedShape,
        });
      // Updating the layer
      map.addLayer({
        id: 'finishedArea',
        source: 'finishedArea',
        type: 'fill',
        paint: {
          'fill-color': '#07BE07',
          'fill-opacity': 0.20,
        },
      });
    }
  }


  getContent() {
    if (this.finishedAreas) {
      return {
        geojson: this.getAll(),
        finishedShape: this.map.getSource('finishedArea')._data,
      };
    }
    return { geojson: this.getAll() };
  }
}

export default E3MapBoxDraw;
