# E3MapBoxDraw

Enhanced version of  [mapbox-gl-draw](https://github.com/mapbox/mapbox-gl-draw), adding some features : 
* **Undo/Redo** (up to 20 operations) : current supported actions : `draw.create`, `draw.update`,`draw.delete`
* **Copy/Paste** : the current behavior is the following : if the user clicked after copying, it will serve as center for the pasting. Otherwise, cliboard will be pasted 45 meters north-east from the original position.
* **UserTracking** : the `user.email` and `user.user` provided will be saved into the feature, along with a timestamp and the action, for each `draw.create` and `draw.update`
* **Finished areas** : the user can mark areas as finished using *SHIFT+ALT* and drag. They will be loaded and saved to the geojson as `.finishedShape`

## API

As an extension of the normal MapBowGL-Draw object, it supports the api described here : [mapbox-gl-draw API](https://github.com/mapbox/mapbox-gl-draw/blob/master/docs/API.md). 

### `addFeatures({map,features,user})`
This function is not mixed with the constructor as it needs the map object, which you can lack when creating the Draw object. Otherwise, you can call it directly after creation. The `features` are the following : 
*`cliboard` : enables the copy/paste
*`history` : enables the undo/redo
*`userTracking` : enables user tracking. If toggled, a `user = {user:"someId",email:"email@example.com"}` must be provided.
*`finishedAreas` : enables the area marking on drag. If toggled, a `finishedAreasSwitch` is provided, allowing the user to switch from adding mode to substract mode.
```js
let Draw = new E3MapBoxDraw()
Draw.addFeatures({map,features:["clipboard","history","userTracking","finishedAreas"],user:{user:"Me",email:"MyEmail"}});
```
---

### `updateGeojson(map,geojson)`
This function should be prefered over manually deleting and adding the features. It basically reset the content, including the finishedAreas layer if toggled. It also adds a filter to remove broken polygons in the file (3 points or less polygons).

---
### `getContent()`
This function should be prefered over `getAll()` as it adds the finishedAreas if toggled.